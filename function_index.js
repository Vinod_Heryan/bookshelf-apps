document.addEventListener("DOMContentLoaded", function () {

  const yearSelect = document.getElementById("year");
  
  const currentYear = new Date().getFullYear();
  
  for (let i = 2000; i <= currentYear; i++) {
    const option = document.createElement("option");
    option.text = i;
    option.value = i;
    yearSelect.appendChild(option);
  }

  const bookForm = document.getElementById("bookForm");
  let books = JSON.parse(localStorage.getItem("books")) || [];

  bookForm.addEventListener("submit", function (event) {
    event.preventDefault();

    const title = document.getElementById("title").value;
    const author = document.getElementById("author").value;
    const year = parseInt(document.getElementById("year").value);
    const isComplete = document.getElementById("isComplete").checked;

    books.push({
      id: Math.floor(new Date().getTime() / 1000), 
      title: title,
      author: author,
      year: year,
      isComplete: isComplete,
    });
    localStorage.setItem("books", JSON.stringify(books));

    resetForm();
    renderBooks();
  });

  function resetForm() {
    document.getElementById("title").value = "";
    document.getElementById("author").value = "";
    document.getElementById("year").value = 0;
    document.getElementById("isComplete").checked = false;
  }

  function renderBooks() {
    const bookListContainerComplete =
      document.getElementById("bookListComplete");
    const bookListContainerNotComplete = document.getElementById(
      "bookListNotComplete"
    );

    bookListContainerComplete.innerHTML = "";
    bookListContainerNotComplete.innerHTML = "";

    const data = JSON.parse(localStorage.getItem("books"));
    if (data && data.length > 0) {
      data.forEach(function (book) {
        const bookItem = document.createElement("div");
        bookItem.classList.add("book-item");
        bookItem.innerHTML = `
            <div class="book-item">
                <p><strong>ID:</strong> ${book.id}</p>
                <p><strong>Judul:</strong> ${book.title}</p>
                <p><strong>Penulis:</strong> ${book.author}</p>
                <p><strong>Tahun Terbit:</strong> ${book.year}</p>
                <p><strong>Selesai:</strong> ${
                  book.isComplete ? "Ya" : "Tidak"
                }</p>
            </div>
            <div class="form-group flex-group" style="margin: 20px;">
            <button style=${
              book.isComplete
                ? "background-color:green"
                : "background-color:blue"
            } onclick="updateBook(${book.id})"><strong>${
          book.isComplete ? "Ubah Belum Selesai" : "Ubah Sudah Selesai"
        }</strong></button>
            <p style="margin: 10px;">  </p>
            <button style="background-color: red;" onclick="deleteBook(${
              book.id
            })"><strong>HAPUS</strong></button>
            </div>
          `;
        if (book.isComplete) {
          bookListContainerComplete.appendChild(bookItem);
        } else {
          bookListContainerNotComplete.appendChild(bookItem);
        }
        if (data.filter((book) => book.isComplete == true).length == 0) {
          const existingMessage = bookListContainerComplete.querySelector("p");
          if (existingMessage) {
            bookListContainerComplete.removeChild(existingMessage);
          }
          const noBooksMessage = document.createElement("p");
          noBooksMessage.textContent = "Belum ada buku tersimpan.";
          bookListContainerComplete.appendChild(noBooksMessage);
        }
        if (data.filter((book) => book.isComplete == false).length == 0) {
          const existingMessage =
            bookListContainerNotComplete.querySelector("p");
          if (existingMessage) {
            bookListContainerNotComplete.removeChild(existingMessage);
          }
          const noBooksMessage = document.createElement("p");
          noBooksMessage.textContent = "Belum ada buku tersimpan.";
          bookListContainerNotComplete.appendChild(noBooksMessage);
        }
      });
    } else {
      const noBooksMessage = document.createElement("p");
      noBooksMessage.textContent = "Belum ada buku tersimpan.";
      bookListContainerComplete.appendChild(noBooksMessage.cloneNode(true));
      bookListContainerNotComplete.appendChild(noBooksMessage);
    }
  }

  renderBooks();
});

function deleteBook(id) {
  const isConfirmed = confirm("Apakah Anda yakin ingin menghapus buku ini?");
  if (isConfirmed) {
    const data = JSON.parse(localStorage.getItem("books"));
    books = data.filter((book) => book.id !== id);
    localStorage.setItem("books", JSON.stringify(books));
    alert("Buku berhasil dihapus.");
    location.reload();
  }
}

function updateBook(id) {
  const isConfirmed = confirm(
    "Apakah Anda yakin ingin mengubah status buku ini?"
  );
  if (isConfirmed) {
    const data = JSON.parse(localStorage.getItem("books"));
    const updatedBooks = data.map((book) => {
      if (book.id === id) {
        book.isComplete = !book.isComplete;
      }
      return book;
    });
    localStorage.setItem("books", JSON.stringify(updatedBooks));
    alert("Buku berhasil diupdate.");
    location.reload();
  }
}
